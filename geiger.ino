#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "Arduino.h"
#include "pms7003.h"
#include "SoftwareSerial.h"

#include <ESP8266TrueRandom.h>


// PMS7003 pins
#define PIN_RX   D2
#define PIN_TX   D1

#define HPIN     D3

#define PM10PIN  D5
#define PM25PIN  D6
#define PM01PIN  D7
#define METERPIN D8

#define MEASURE_INTERVAL_MS 10000
#define UPDATE_MS 1000

static SoftwareSerial sensor(PIN_RX, PIN_TX);

long r;
float pl = 10;
float npl = 0;
float pdiff = 0;

int use_pm10 = LOW;
int use_pm25 = HIGH;
int use_pm01 = HIGH;

typedef struct {
    float pm10;
    float pm2_5;
    float pm1_0;
} pms_dust_t;

void setup(void)
{
    uint8_t txbuf[8];
    int txlen;

    // welcome message
    Serial.begin(115200);
    Serial.println("PMS7003 ESP reader");

    // initialize the sensor
    sensor.begin(9600);
    txlen = PmsCreateCmd(txbuf, sizeof(txbuf), PMS_CMD_ON_STANDBY, 1);
    sensor.write(txbuf, txlen);
    txlen = PmsCreateCmd(txbuf, sizeof(txbuf), PMS_CMD_AUTO_MANUAL, 1);
    sensor.write(txbuf, txlen);
    PmsInit();

    pinMode(HPIN,OUTPUT);
    digitalWrite(HPIN,LOW);

    pinMode(PM10PIN,INPUT_PULLUP);
    pinMode(PM25PIN,INPUT_PULLUP);
    pinMode(PM01PIN,INPUT_PULLUP);

    pinMode(METERPIN,OUTPUT);
    
    Serial.println("setup() done");
}

static void send_json(const pms_dust_t *pms)
{
   char tmp[128];
    
    // PMS7003
    if (pms != NULL) {
        // AMB, "standard atmosphere" particle
        sprintf(tmp, "pm10:%.2f, pm2_5:%.2f, pm1_0:%.2f",pms->pm10, pms->pm2_5, pms->pm1_0);
    }
    Serial.println(tmp);
}

void loop(void)
{
    static pms_dust_t pms_meas_sum = {0.0, 0.0, 0.0};
    static int pms_meas_count = 0;
    static unsigned long last_sent = 0;
    static unsigned long ls2 = 0;
    char pmode[60];
    
    
    r = ESP8266TrueRandom.rand();
    if (r < pl) {
      digitalWrite (HPIN, HIGH);
      delay(5);
      digitalWrite (HPIN, LOW);  
    }

    // check measurement interval
    unsigned long ms = millis();
    if ((ms - last_sent) > MEASURE_INTERVAL_MS) {
        if (pms_meas_count > 0) {
            // average dust measurement
            pms_meas_sum.pm10 /= pms_meas_count;
            pms_meas_sum.pm2_5 /= pms_meas_count;
            pms_meas_sum.pm1_0 /= pms_meas_count;

            if (use_pm10 == 0) {
              npl = pms_meas_sum.pm10 * 5;
            }
            else if (use_pm25 == 0) {
              npl = pms_meas_sum.pm2_5 * 5;
            }
            else if (use_pm01 == 0) {
              npl = pms_meas_sum.pm1_0 * 5;
            }
            else {
              npl = pms_meas_sum.pm10 * 5;
            }
            
            Serial.println(npl);
            // publish it
            send_json(&pms_meas_sum);
            pdiff = abs(npl - pl) / 10;
            Serial.println(pdiff);
                    
            // reset sum
            pms_meas_sum.pm10 = 0.0;
            pms_meas_sum.pm2_5 = 0.0;
            pms_meas_sum.pm1_0 = 0.0;
            pms_meas_count = 0;
        } else {
            Serial.println("Not publishing, no measurement received from PMS7003!");
            
            // publish only the alive counter
            send_json(NULL);
        }
        last_sent = ms;
    }

    unsigned long ms2 = millis();
    if ((ms2 - ls2) > UPDATE_MS) {
      if (npl > pl) {
        pl += pdiff;
      }
      if (npl < pl) {
        pl -= pdiff;
      }
      Serial.println(pl);        
      ls2 = ms2;
      use_pm10 = digitalRead(PM10PIN);
      use_pm25 = digitalRead(PM25PIN);
      use_pm01 = digitalRead(PM01PIN);
      sprintf(pmode,"PM10: %i, PM25: %i, PM01: %i",use_pm10,use_pm25,use_pm01);
      Serial.println(pmode);
      analogWrite(METERPIN,pl);
    }
    

    // check for incoming measurement data
    while (sensor.available()) {
        uint8_t c = sensor.read();
        if (PmsProcess(c)) {
            // parse it
            pms_meas_t pms_meas;
            PmsParse(&pms_meas);
            // sum it
            pms_meas_sum.pm10 += pms_meas.concPM10_0_amb;
            pms_meas_sum.pm2_5 += pms_meas.concPM2_5_amb;
            pms_meas_sum.pm1_0 += pms_meas.concPM1_0_amb;
            pms_meas_count++;
        }
    }
}
