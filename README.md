WAT
===

A PMS7003 particle dust sensor hooked to some hacky code to make Geiger noises.

WHY?
====

My vague theory was that while you could generate graphs and heatmaps of pollution, and have people nod along with your
explanation and go 'Dear me yes that's terrible something must be done..', if you _really_ wanted to put the fear of
imminent death into them, you needed it to make noises generally connected to imminent death.

Hence a Geiger counter.

DID IT WORK?
============

Ho fuck yes it did. Mind, it works best on Gen X because we grew up with that sort of thing.


Docs, of a sort: https://remarkably.placid.horse/
